﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        IWebDriver driver;

        [TestMethod]
        public void TestMethod1()
        {
            ChromeOptions options = new ChromeOptions();

            driver = new ChromeDriver("C:\\prog", options);

       
       
            driver.Navigate().GoToUrl("https://www.seleniumeasy.com/test/basic-first-form-demo.html");


            driver.Manage().Window.Maximize();

            Thread.Sleep(TimeSpan.FromSeconds(3));

            // step#1 cancel a blocking popup
            IWebElement CloseNoThanks = driver.FindElement(By.XPath("//*[@id='at-cv-lightbox-close']"));
            CloseNoThanks.Click();

            // step#2 enter msg into text box
            IWebElement EnterMsg = driver.FindElement(By.XPath("//input[@id='user-message']"));
            string expectedString = "testing";
            EnterMsg.SendKeys(expectedString);
            //Thread.Sleep(TimeSpan.FromSeconds(3));

            // step#3 click showmsg button 
            IWebElement ShowMsg = driver.FindElement(By.XPath("//*[@id='get-input']/button"));
            ShowMsg.Click();
            Thread.Sleep(TimeSpan.FromSeconds(3));

        

            //// step#4 get msg from webpage label and compare 
            IWebElement displayMsg = driver.FindElement(By.XPath("//*[@id='display']"));
            string actualMsg = displayMsg.Text;
           
            //Thread.Sleep(TimeSpan.FromSeconds(3));

            Assert.AreEqual(expectedString, actualMsg);

            //Closing and quitting driver
            driver.Close();
            driver.Quit();


        }

        
    }

}
